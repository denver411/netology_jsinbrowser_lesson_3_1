'use strict'

const container = document.querySelector('#tabs');
const tabsSection = container.querySelector('.tabs-nav');
const ContentArray = Array.from(container.querySelector('.tabs-content').children);

//tabs create

for (let i = 0; i < ContentArray.length - 1; i++) {
  tabsSection.appendChild(tabsSection.children[i].cloneNode(true));
}

//active tab
tabsSection.firstElementChild.classList.add('ui-tabs-active');

//tabs titles & pics
ContentArray.forEach((item, index) => {
  tabsSection.children[index].firstChild.innerText = item.dataset.tabTitle;
  tabsSection.children[index].firstChild.classList.add(item.dataset.tabIcon);
})

//content visibility
function checkContent() {
  ContentArray.forEach((item, index) => {
    tabsSection.children[index].classList.contains('ui-tabs-active') ?
      item.classList.remove('hidden') : item.classList.add('hidden');
  });
}

checkContent();

//onclick event
Array.from(tabsSection.children).forEach(item => {
  item.addEventListener('click', checkTab);
});


function checkTab(event) {
  Array.from(tabsSection.children).forEach(item => {
    item.classList.remove('ui-tabs-active');
  });
  event.currentTarget.classList.add('ui-tabs-active');
  checkContent();
}

