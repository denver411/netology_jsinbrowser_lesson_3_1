'use strict'

function slider(container) {
  const slideButtons = container.querySelector('.slider-nav');
  let currentSlide = container.querySelector('.slides').firstElementChild;
  currentSlide.classList.add('slide-current')
  const firstSlide = container.querySelector('.slides').firstElementChild;
  const lastSlide = container.querySelector('.slides').lastElementChild;

  slideButtons.addEventListener('click', event => {
    changeSlide(event.target);
  })

  function updateSlideButtons() {
    const prevBtn = container.querySelector('[data-action = "prev"]');
    const nextBtn = container.querySelector('[data-action = "next"]');
    const firstBtn = container.querySelector('[data-action = "first"]');
    const lastBtn = container.querySelector('[data-action = "last"]');

    currentSlide.nextElementSibling ?
      nextBtn.classList.remove('disabled') : nextBtn.classList.add('disabled');
    currentSlide.nextElementSibling ?
      lastBtn.classList.remove('disabled') : lastBtn.classList.add('disabled');
    currentSlide.previousElementSibling ?
      prevBtn.classList.remove('disabled') : prevBtn.classList.add('disabled');
    currentSlide.previousElementSibling ?
      firstBtn.classList.remove('disabled') : firstBtn.classList.add('disabled');
  }

  function changeSlide(event) {
    if (!event.classList.contains('disabled')) {
      currentSlide = container.querySelector('.slide-current');
      currentSlide.classList.remove('slide-current');
      switch (event.dataset.action) {
        case 'next':
          currentSlide = currentSlide.nextElementSibling;
          break;
        case 'prev':
          currentSlide = currentSlide.previousElementSibling;
          break;
        case 'last':
          currentSlide = lastSlide;
          break;
        case 'first':
          currentSlide = firstSlide;
          break;
      }
      currentSlide.classList.add('slide-current');;
      updateSlideButtons();
    }
  }

  updateSlideButtons();

}

const sliders = document.querySelectorAll('.slider');
Array.from(sliders).forEach(item => slider(item));