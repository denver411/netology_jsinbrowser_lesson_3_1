'use strict'

function toDo(container) {

  const listItems = container.querySelectorAll('label');
  const doneSection = container.querySelector('.done');
  const undoneSection = container.querySelector('.undone');

  function listItemsAction(event) {
    if (event.target.tagName === 'LABEL') {
      if (event.target.firstElementChild.checked) {
        undoneSection.appendChild(event.target);
      } else {
        doneSection.appendChild(event.target);
      }
    }
  }

  Array.from(listItems).forEach(item => {
    item.addEventListener('click', listItemsAction)
  })
}

const toDoLists = document.querySelectorAll('.todo-list');
Array.from(toDoLists).forEach(item => {
  toDo(item);
})